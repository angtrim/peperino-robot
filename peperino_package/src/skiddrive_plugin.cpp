// Gazebo
#include <gazebo/common/common.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo_plugins/gazebo_ros_utils.h>

// ROS
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose2D.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>

// Custom Callback Queue
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>

// Boost
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <gazebo/math/gzmath.hh>
#include <sdf/sdf.hh>




namespace gazebo{

	class SkidDrivePlugin : public ModelPlugin{

  		// Pointer to the model
		private: physics::ModelPtr model;
 		// Pointer to the joints, the names are got from the model xml 
		private: physics::JointPtr fr_joint;
		private: physics::JointPtr fl_joint;
		private: physics::JointPtr rr_joint;
		private: physics::JointPtr rl_joint;
		private: GazeboRosPtr gazebo_ros_;
		private: double velRight = 0;
		private: double velLeft = 0;
		private: boost::shared_ptr<ros::NodeHandle> subscriberNode;
		private: boost::shared_ptr<ros::NodeHandle> publisherNode;
		private: boost::shared_ptr<ros::NodeHandle> turtleNode;
		private: double last_odomMsgupdate;
		private: double wheel_separation = 0.495;
		private: geometry_msgs::TwistStamped velMsg;
 		//Main reference of the node
		private: ros::Publisher velPublisher;
		private: ros::Subscriber cmdVelSubscriber;
		private: ros::Subscriber turtleSubscriber;
		private: tf::TransformBroadcaster transformBroadcaster;
    	// Pointer to the update event connection
		private: event::ConnectionPtr updateConnection;


		public: void Load(physics::ModelPtr parent, sdf::ElementPtr sdf){
			ROS_INFO("Loading skid drive plugin");

      	// Store the pointer to the model
			this->model = parent;

 			// ROS node
			subscriberNode = boost::shared_ptr<ros::NodeHandle> ( new ros::NodeHandle ( "") );
			publisherNode = boost::shared_ptr<ros::NodeHandle> ( new ros::NodeHandle ( "peperino/") );
			turtleNode = boost::shared_ptr<ros::NodeHandle> ( new ros::NodeHandle ( "turtle1/") );

    		// Make sure the ROS node for Gazebo has already been initialized
			ros::SubscribeOptions so =
			ros::SubscribeOptions::create<geometry_msgs::Twist>("cmd_vel", 1,
				boost::bind(&SkidDrivePlugin::cmdVelCallback, this, _1),
				ros::VoidPtr(), NULL);
			cmdVelSubscriber = subscriberNode->subscribe(so);
			velPublisher = publisherNode->advertise<geometry_msgs::TwistStamped>("vel", 1);

    		// subscribe to turtle
			ros::SubscribeOptions so1 =
			ros::SubscribeOptions::create<geometry_msgs::Twist>("cmd_vel", 1,
				boost::bind(&SkidDrivePlugin::cmdVelCallback, this, _1),
				ros::VoidPtr(), NULL);
			turtleSubscriber = turtleNode->subscribe(so1);


			initModelJoints(sdf);
      	// Listen to the update event. This event is broadcast every
      	// simulation iteration.
			this->updateConnection = event::Events::ConnectWorldUpdateBegin(
				boost::bind(&SkidDrivePlugin::OnUpdate, this, _1));
		}

	   // Called by the world update start event
	 public: void OnUpdate(const common::UpdateInfo & /*_info*/){
		setLeftWheelsVelocity(velLeft);
		setRightWheelsVelocity(velRight);
		publishVel();
		logWorldPosition();
	}

	public: void cmdVelCallback ( const geometry_msgs::Twist::ConstPtr& cmd_msg ){
		double x = cmd_msg->linear.x;
		double rotation = cmd_msg->angular.z;
		    // ROS_INFO("\nLinear :%f",x);
		    // ROS_INFO("\nRotation :%f",rotation);
		this->velLeft = (x - rotation * wheel_separation / 2.0)*2;
		this->velRight = (x + rotation * wheel_separation / 2.0)*2;
	}


	public: void initModelJoints(sdf::ElementPtr sdf){
	 		// Get the names
		std::string fr_joint_name = "front_right_wheel_joint";
		std::string fl_joint_name = "front_left_wheel_joint";
		std::string rr_joint_name = "rear_right_wheel_joint";
		std::string rl_joint_name = "rear_left_wheel_joint";
	 		// xml tag 
		if(sdf->HasElement("front_right_wheel_joint"))
			sdf->GetElement("front_right_wheel_joint")->ToString(fr_joint_name);
		if(sdf->HasElement("front_left_wheel_joint"))
			sdf->GetElement("front_left_wheel_joint")->ToString(fl_joint_name);
		if(sdf->HasElement("rear_right_wheel_joint"))
			sdf->GetElement("rear_right_wheel_joint")->ToString(rr_joint_name);
		if(sdf->HasElement("rear_left_wheel_joint"))
			sdf->GetElement("rear_left_wheel_joint")->ToString(rl_joint_name);

		    // Get the joints
		this->fr_joint = this->model->GetJoint(fr_joint_name);
		this->fl_joint = this->model->GetJoint(fl_joint_name);
		this->rr_joint = this->model->GetJoint(rr_joint_name);
		this->rl_joint = this->model->GetJoint(rl_joint_name);
	}

	public: void setRightWheelsVelocity(double velocity){
	 		// if velocity > 0 robot move forward
		this->fr_joint->SetVelocity(0,velocity);
		this->rr_joint->SetVelocity(0,velocity);
	}

	public: void setLeftWheelsVelocity(double velocity){
	 		// if velocity > 0 robot move forward
		this->fl_joint->SetVelocity(0,velocity);
		this->rl_joint->SetVelocity(0,velocity);
	}

	public: void publishVel(){
		double wheel_diameter = 0.145;
	 	// Joints velocity
		double vl =  fl_joint->GetVelocity(0);
		double vr =  fr_joint->GetVelocity(0);
		ros::Time currentRosTime = ros::Time::now();
		double current_time = currentRosTime.toSec();
		double seconds_since_last_update = (  current_time - last_odomMsgupdate );
		last_odomMsgupdate = current_time;
		double b = wheel_separation;

		double sl = vl * ( wheel_diameter / 2.0 );
		double sr = vr * ( wheel_diameter / 2.0 );
		double ssum = sl + sr;
		double sdiff = sr - sl ;
		double linearSpeed = ssum/2;
		double rotationalSpeed = sdiff/wheel_separation;
		velMsg.twist.linear.x = linearSpeed;
		velMsg.twist.angular.z = rotationalSpeed;
		velMsg.header.stamp =  currentRosTime;
		velMsg.header.frame_id = "map";
		velPublisher.publish ( velMsg );

			  // ROS_INFO("\nVl :%f",vl);
			  //   ROS_INFO("\nVr :%f",vr);
			  //   ROS_INFO("\nRotational :%f",rotationalSpeed);
			  //   ROS_INFO("\nEuler X :%f",odomX);
			  //   ROS_INFO("\nEuler Y :%f",odomY);
			  //   ROS_INFO("\nEuler Theta :%f",odomTheta);
	}

	public: void logWorldPosition(){
		math::Pose pose = this->model->GetWorldPose();
		tf::Transform transform;
		transform.setOrigin( tf::Vector3(pose.pos.x, pose.pos.y, 0.0) );
		tf::Quaternion q;
		q.setRPY(0, 0, pose.rot.GetYaw());
		transform.setRotation(q);
		transformBroadcaster.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "map", "peperino_gt"));
	}


};




	  	// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(SkidDrivePlugin);
}



