#include <ros/ros.h>
#include <signal.h>
#include <termios.h>
#include <stdio.h>
#include <iostream>
#include <peperino_package/CameraTiltMsg.h>
#define KEYCODE_R 0x43 
#define KEYCODE_L 0x44
#define KEYCODE_U 0x41
#define KEYCODE_D 0x42
#define KEYCODE_Q 0x71
#define KEYCODE_i 0x69
#define KEYCODE_a 0x61
#define KEYCODE_b 0x62
#define KEYCODE_c 0x63
#define KEYCODE_d 0x64
#define KEYCODE_e 0x65
#define KEYCODE_f 0x66
#define KEYCODE_g 0x67
#define KEYCODE_h 0x68
#define KEYCODE_i 0x69
#define KEYCODE_j 0x6a
#define KEYCODE_k 0x6b
#define KEYCODE_l 0x6c
#define KEYCODE_m 0x6d
#define KEYCODE_n 0x6e
#define KEYCODE_o 0x6f
#define KEYCODE_p 0x70
#define KEYCODE_q 0x71
#define KEYCODE_r 0x72
#define KEYCODE_s 0x73
#define KEYCODE_t 0x74
#define KEYCODE_u 0x75
#define KEYCODE_v 0x76
#define KEYCODE_w 0x77
#define KEYCODE_x 0x78
#define KEYCODE_y 0x79
#define KEYCODE_z 0x7a

class TeleopCamera
{
public:
  TeleopCamera();
  void keyLoop();

private:

  
  ros::NodeHandle nodeHandle;
  ros::Publisher cameraTiltPublisher;
  
};

TeleopCamera::TeleopCamera()
{
  cameraTiltPublisher = nodeHandle.advertise<peperino_package::CameraTiltMsg>("peperino/camera_tilt", 1);
}


int kfd = 0;
struct termios cooked, raw;

void quit(int sig)
{

  (void)sig;
  tcsetattr(kfd, TCSANOW, &cooked);
  ros::shutdown();
  exit(0);
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "camera_teleop");
  TeleopCamera teleopCamera;

  signal(SIGINT,quit);

  teleopCamera.keyLoop();
  
  return(0);
}


void TeleopCamera::keyLoop()
{



  for(;;){
    int i;
    double radiansV;
    double radiansH;

    std::cout << "Enter the value of the vertical tilt in radians (MAX 0.785398 -> Camera down)" << "\n";
    std::cin >> radiansV;
    std::cout << "Enter the value of the horizontal tilt in radians (MAX 0.785398 -> Camera left)" << "\n";
    std::cin >> radiansH;
    if(radiansH > 0.785398 || radiansV > 0.785398){
      std::cout << "INVALID VALUES" << "\n";
      std::cout << "---------------------------" << "\n";
       
    }else{
    peperino_package::CameraTiltMsg cameraTiltMsg;
    cameraTiltMsg.hradtilt = radiansH;
    cameraTiltMsg.vradtilt = radiansV;
   
    cameraTiltPublisher.publish ( cameraTiltMsg );

    std::cout << "Camera moved" << "\n";
    std::cout << "---------------------------" << "\n";
    }

       

  }


  return;
}


