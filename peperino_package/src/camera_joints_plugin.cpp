// Gazebo
#include <gazebo/common/common.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo_plugins/gazebo_ros_utils.h>

// ROS
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose2D.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>

// Custom Callback Queue
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>

// Boost
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <gazebo/math/gzmath.hh>
#include <sdf/sdf.hh>
#include <peperino_package/CameraTiltMsg.h>
#include <tf/tf.h>


 namespace gazebo{

 	class CameraJointsPlugin : public ModelPlugin{

  		// Pointer to the model
 		private: physics::ModelPtr model;
 		// Pointer to the joints, the names are got from the model xml 
 		private: physics::JointPtr h_joint;
 		private: physics::JointPtr v_joint;
 	   private: physics::LinkPtr h_joint_link;
 	   private: physics::LinkPtr v_joint_link;
 		private: physics::JointController* jointController;
 		private: common::PID* jointPID;
 		private: bool moved = false;
 		private: boost::shared_ptr<ros::NodeHandle> subscriberNode;
		private: ros::Subscriber cameraTiltSubscriber;
		private: tf::TransformBroadcaster transformBroadcaster;
		private:tf::TransformListener listener;
	   private: double verticalTilt = 0;
	   private: double horizTilt = 0;


    	// Pointer to the update event connection
 		private: event::ConnectionPtr updateConnection;


	    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr sdf){
			ROS_INFO("Loading camera plugin");

      		// Store the pointer to the model
 			this->model = _parent;
 			this->jointController = new physics::JointController(this->model);
 			this-> jointPID  = new common::PID(1, 0.1, 0.01, 1, -1, 1, -1);
 			initModelJoints(sdf);

 			subscriberNode = boost::shared_ptr<ros::NodeHandle> ( new ros::NodeHandle ( "peperino/") );
 			ros::SubscribeOptions so =
        	ros::SubscribeOptions::create<peperino_package::CameraTiltMsg>("camera_tilt", 1,
                boost::bind(&CameraJointsPlugin::cameraTiltCallback, this, _1),
                ros::VoidPtr(), NULL);
    		cameraTiltSubscriber = subscriberNode->subscribe(so);



      		// Listen to the update event. This event is broadcast every
      		// simulation iteration.
 			this->updateConnection = event::Events::ConnectWorldUpdateBegin(
 				boost::bind(&CameraJointsPlugin::OnUpdate, this, _1));
	 	}

	    // Called by the world update start event
	    public: void OnUpdate(const common::UpdateInfo & /*_info*/){
		 	math::Pose intVPose = this->v_joint->GetWorldPose();
		 	math::Pose intHPose = this->h_joint->GetWorldPose();


	 		// Apply torque to reach the position (1 degree of error )
	 		if(v_joint->GetAngle(0).Radian() > verticalTilt + 0.0174533 && v_joint->GetVelocity(0) < 0.1){
	 			v_joint->SetForce(0,-10.1);
	 		}else if(v_joint->GetAngle(0).Radian() < verticalTilt - 0.0174533 && v_joint->GetVelocity(0) < 0.1){
	 			v_joint->SetForce(0,10.5);
	 		}else{
	 			v_joint->SetForce(0,0);
	 		}


	 		// Apply torque to reach the position (1 degree of error )
	 		if(h_joint->GetAngle(0).Radian() > horizTilt + 0.0174533 && h_joint->GetVelocity(0) < 0.1){
	 			h_joint->SetForce(0,-10.3);
	 		}else if(h_joint->GetAngle(0).Radian() < horizTilt - 0.0174533 && h_joint->GetVelocity(0) < 0.1){
	 			h_joint->SetForce(0,10.3);
	 		}else{
	 			h_joint->SetForce(0,0);
	 		}



	 		bool canTransform = this->listener.canTransform("base_link","map",ros::Time(0));
	 		if(canTransform){
		 		publishTransform(intVPose,"v_camera_stick_joint");
		 		publishTransform(intHPose,"h_camera_stick_joint");

	 		}
	 		
	 	}

	   public: void publishTransform(math::Pose pose, std::string jointName){
	   		tf::Transform Ytransform;
	    		Ytransform.setOrigin( tf::Vector3(pose.pos.x, pose.pos.y, pose.pos.z) );
	    		tf::Quaternion q;
	    		q.setRPY(pose.rot.GetRoll(),pose.rot.GetPitch(), pose.rot.GetYaw());
	    		Ytransform.setRotation(q);
	    		tf::Stamped< tf::Pose > stampedYinWorld = tf::Stamped<tf::Pose>(Ytransform,ros::Time(0),"map");	
	    		tf::Stamped< tf::Pose > stampedYinBase;	
	   
	 			this->listener.transformPose("base_link",stampedYinWorld,stampedYinBase);
	 			geometry_msgs::PoseStamped pose_msg;
	 			tf::poseStampedTFToMsg (stampedYinBase, pose_msg);

	 			tf::Transform transform_auxiliar;
				transform_auxiliar.setOrigin(tf::Vector3(pose_msg.pose.position.x, pose_msg.pose.position.y, pose_msg.pose.position.z));
				transform_auxiliar.setRotation(tf::Quaternion( pose_msg.pose.orientation.x, pose_msg.pose.orientation.y, pose_msg.pose.orientation.z, pose_msg.pose.orientation.w));
			   transformBroadcaster.sendTransform(tf::StampedTransform(transform_auxiliar, ros::Time::now(), "base_link", jointName));
	   }


	 	public: void initModelJoints(sdf::ElementPtr sdf){
	 		// Get the names
	 		std::string joint1_name = "h_camera_stick_joint";
	 		std::string joint2_name = "v_camera_stick_joint";
	 		this->h_joint = this->model->GetJoint(joint1_name);
		   this->v_joint = this->model->GetJoint(joint2_name);
		   // this->h_joint_link = this->model->GetJoint(joint1_name);
		   // this->v_joint_link = this->model->GetJoint(joint2_name);
	 	}


	 	public: void cameraTiltCallback ( const peperino_package::CameraTiltMsg::ConstPtr& cameraMsg ){
	 		horizTilt = cameraMsg->hradtilt;
	 		verticalTilt = cameraMsg->vradtilt;
		}






	 	};

	  	// Register this plugin with the simulator
	 	GZ_REGISTER_MODEL_PLUGIN(CameraJointsPlugin);
 }



