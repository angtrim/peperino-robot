#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <math.h>
#include <cmath>  
#include <string>
#include <ros/ros.h>
#include <fstream>
#include <vector>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <peperino_package/CameraTiltMsg.h>
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
#define RUN_PERIOD_DEFAULT 0.1
#define NAME_OF_THIS_NODE "patrolling_node"

class ROSnode {
private: 
  int index;
  std::vector<float> xS;  
  std::vector<float> yS; 
  std::vector<float> wS; 
  std::vector<float> cHS; 
  std::vector<float> cVS; 
  ros::NodeHandle nodeHandle;
  ros::Publisher cameraTiltPublisher;

public:
    bool Prepare();
    void RunContinuously();
    void Shutdown();
};

//-----------------------------------------------------------------
//-----------------------------------------------------------------

bool ROSnode::Prepare() {
    ROS_INFO("%s", ros::this_node::getName().c_str());
    cameraTiltPublisher = nodeHandle.advertise<peperino_package::CameraTiltMsg>("peperino/camera_tilt", 1);
    ROS_INFO("Node %s ready to run.", ros::this_node::getName().c_str());
    // Read the position
    std::fstream myfile("patrolling.txt", std::ios_base::in);
    ROS_INFO("reading");
    float x,y,yaw,ch,cv;
    while (myfile >> x >> y >> yaw >> ch >> cv)
    {
         ROS_INFO("X %f", x);
         ROS_INFO("Y %f", y);
         ROS_INFO("yaw %f", yaw);
         ROS_INFO("Camera h %f", ch);
         ROS_INFO("Camera v %f", cv);
         ROS_INFO("-----");
         xS.push_back(x);
         yS.push_back(y);
         wS.push_back(yaw);
         cHS.push_back(ch);
         cVS.push_back(cv);
    }
    index = 0;
    return true;
}


void ROSnode::RunContinuously() {
  //tell the action client that we want to spin a thread by default
  MoveBaseClient ac("move_base", true);

  //wait for the action server to come up
  while(!ac.waitForServer(ros::Duration(5.0))){
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  while(true){
    move_base_msgs::MoveBaseGoal goal;

    //we'll send a goal to the robot to move 1 meter forward
    goal.target_pose.header.frame_id = "map";
    goal.target_pose.header.stamp = ros::Time::now();
    tf::Quaternion qt;

    qt.setRPY ( 0,0, wS.at(index) );

    goal.target_pose.pose.position.x = xS.at(index);
    goal.target_pose.pose.position.y = yS.at(index);
    goal.target_pose.pose.orientation.x = qt.x();
    goal.target_pose.pose.orientation.y = qt.y();
    goal.target_pose.pose.orientation.z = qt.z();
    goal.target_pose.pose.orientation.w = qt.w();
    ROS_INFO("Sending goal");
    ac.sendGoal(goal);
    ac.waitForResult();

    if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){

      ROS_INFO("Hooray, goal reached. Moving camera");
      peperino_package::CameraTiltMsg cameraTiltMsg;

      cameraTiltMsg.hradtilt = cHS.at(index);
      cameraTiltMsg.vradtilt = cVS.at(index);
      cameraTiltPublisher.publish ( cameraTiltMsg );
      ros::Duration(5).sleep(); // sleep for half a second
    }
    else{
      ROS_INFO("The base failed to move forward 1 meter for some reason");
    }


    if(index < xS.size() - 1){
      index++;
    }else{
      index = 0;
    }
    ros::spinOnce();
  }

}

void ROSnode::Shutdown() {
  ROS_INFO("Node %s shutting down.", ros::this_node::getName().c_str());
}



int main(int argc, char **argv) {
  ros::init(argc, argv, NAME_OF_THIS_NODE);
  
  ROSnode mNode;

  if(!mNode.Prepare()) return (-1);
  mNode.RunContinuously();
  mNode.Shutdown();
  
  return (0);
}