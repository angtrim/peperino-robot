#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <math.h>
#include <cmath>  
#include <string>
#define RUN_PERIOD_DEFAULT 0.1
#define NAME_OF_THIS_NODE "odom_node"

class ROSnode {
private: 
    ros::NodeHandle Handle;
    ros::Subscriber velSub;
    ros::Subscriber initPoseSub;
    ros::Publisher odomPub;
    nav_msgs::Odometry odomMsg;
    tf::TransformBroadcaster transformBroadcaster;

    double odomX ;
    double odomY ;
    double odomTheta;
    double t;
    std::string method;
        
    void velCallback(const geometry_msgs::TwistStamped::ConstPtr& msg);
    void initPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);

public:
    bool Prepare();
    void RunContinuously();
    void Shutdown();
};

//-----------------------------------------------------------------
//-----------------------------------------------------------------

bool ROSnode::Prepare() {
    ROS_INFO("%s", ros::this_node::getName().c_str());
    if (!Handle.getParam(ros::this_node::getName()+"/method", method)){
        method = "kutta";
    }
    // Subscribe to a topic that has the velocity of the robot and the simulator pose (for debugging purpose)
    velSub = Handle.subscribe("/peperino/vel", 10, &ROSnode::velCallback, this);
    odomPub = Handle.advertise<nav_msgs::Odometry>("/odom", 10);
    initPoseSub = Handle.subscribe("/initialpose",10,&ROSnode::initPoseCallback, this);
    odomX  = 0;
    odomY = 0;
    odomTheta = 0;
    t = 0;

    ROS_INFO("Node %s ready to run.", ros::this_node::getName().c_str());
    ROS_INFO("Odometry method : %s",method.c_str());
    return true;
}


void ROSnode::RunContinuously() {
  ROS_INFO("Node %s running continuously.", ros::this_node::getName().c_str());

  ros::spin();
}

void ROSnode::Shutdown() {
  ROS_INFO("Node %s shutting down.", ros::this_node::getName().c_str());
}

void ROSnode::initPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg){
    odomX = msg->pose.pose.position.x;
    odomY = msg->pose.pose.position.y;
    tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    odomTheta = yaw;
}


void ROSnode::velCallback(const geometry_msgs::TwistStamped::ConstPtr& msg) {
    if(t < 0) {
        t = msg->header.stamp.toSec();
        return;
    }
    double dt = msg->header.stamp.toSec() - t;
    float linearSpeed = msg->twist.linear.x;
    float rotationalSpeed = msg->twist.angular.z;


    if(method == "kutta"){
        double dx = linearSpeed*dt*cos(odomTheta + rotationalSpeed*dt/2); 
        double dy = linearSpeed*dt*sin(odomTheta + rotationalSpeed*dt/2); 
        odomX = odomX + dx;
        odomY = odomY + dy;
    }else if(method == "euler"){
        // Euler
        double dx = linearSpeed*dt*cos(odomTheta); 
        double dy = linearSpeed*dt*sin(odomTheta); 
        odomX = odomX + dx;
        odomY = odomY + dy;
    }else{
        // Exact
        double dx;
        double dy;
        if(abs(rotationalSpeed) < 0.1){
            dx = linearSpeed*dt*cos(odomTheta); 
            dy = linearSpeed*dt*sin(odomTheta); 
            odomX = odomX + dx;
            odomY = odomY + dy;
        }else{
            dx = linearSpeed/rotationalSpeed*(sin(odomTheta + rotationalSpeed*dt) - sin(odomTheta)); 
            dy = linearSpeed/rotationalSpeed*(cos(odomTheta + rotationalSpeed*dt) - cos(odomTheta)); 
            odomX = odomX + dx;
            odomY = odomY - dy;
        }
    }
    odomTheta = odomTheta + rotationalSpeed*dt;



    tf::Quaternion qt;
    tf::Vector3 vt;
    qt.setRPY ( 0,0, odomTheta );
    vt = tf::Vector3 ( odomX, odomY, 0 );


    // Publish on tf
    tf::Transform transform;
    transform.setOrigin( vt);
    transform.setRotation(qt);
    transformBroadcaster.sendTransform(tf::StampedTransform(transform, msg->header.stamp, "odom", "base_link"));

    // Publish on topic
    odomMsg.pose.pose.position.x = vt.x();
    odomMsg.pose.pose.position.y = vt.y();
    odomMsg.pose.pose.position.z = vt.z();

    odomMsg.pose.pose.orientation.x = qt.x();
    odomMsg.pose.pose.orientation.y = qt.y();
    odomMsg.pose.pose.orientation.z = qt.z();
    odomMsg.pose.pose.orientation.w = qt.w();

    odomMsg.twist.twist.angular.z = rotationalSpeed;
    odomMsg.twist.twist.linear.x = linearSpeed;
    odomMsg.twist.twist.linear.y = 0;

    // set covariance
    odomMsg.pose.covariance[0] = 0.00001;
    odomMsg.pose.covariance[7] = 0.00001;
    odomMsg.pose.covariance[14] = 1000000000000.0;
    odomMsg.pose.covariance[21] = 1000000000000.0;
    odomMsg.pose.covariance[28] = 1000000000000.0;
    odomMsg.pose.covariance[35] = 0.001;


    // set header
    odomMsg.header.stamp =  msg->header.stamp;
    odomMsg.header.frame_id = "odom";
    odomMsg.child_frame_id = "base_link";
    odomPub.publish ( odomMsg );
    
    // Set new time
    t = msg->header.stamp.toSec();
}

int main(int argc, char **argv) {
  ros::init(argc, argv, NAME_OF_THIS_NODE);
  
  ROSnode mNode;

  if(!mNode.Prepare()) return (-1);
  mNode.RunContinuously();
  mNode.Shutdown();
  
  return (0);
}